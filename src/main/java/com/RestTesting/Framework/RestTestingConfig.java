package com.RestTesting.Framework;

import io.restassured.RestAssured;
import org.testng.annotations.BeforeSuite;

public class RestTestingConfig {
    @BeforeSuite(alwaysRun = true)
    public void config(){
        RestAssured.baseURI = "http://www.musicpost.biz";

    }
}

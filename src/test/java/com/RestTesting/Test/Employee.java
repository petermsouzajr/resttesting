package com.RestTesting.Test;

import com.RestTesting.Test.common.EndPoint;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.json.simple.JSONObject;
import org.testng.Assert;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.given;

public class Employee {

    @Test(groups = "nocreds")
    public void validateemployee () {
        given()
                    .auth()
                    . preemptive()
                    .basic("guest", "guest")
                .when()
                    .get("http://www.musicpost.biz/")
                .then()
                    .statusCode(200).log().all();
    }

    @Test(groups = "creds")
    public void validateemployee2 () {
        given()
                    .auth()
                    .basic("guest", "guest")
                .when()
                    .get(EndPoint.GET_USERNAME)
                .then()
                    //.body("id", equalTo("postcontainer_1850"))
                    .statusCode(200).log().all();
    }

    @Test(groups = "JSONput")
    public void validateEmployee3 () {
        RequestSpecification request= RestAssured.given();

        request.header("Content-Type","application/json");

        JSONObject json=new JSONObject();
        json.put("id","24");
        json.put("title","fourthInsertTitle");
        json.put("author","fourthInsertAuthor");

        request.body(json.toJSONString());

        Response response=request.post("http://localhost:3000/posts");

        int code=response.getStatusCode();

        Assert.assertEquals(code, 201);
    }

    @Test(groups = "JSONget")
    public void validateEmployee4 () {
        given()
                .get("http://localhost:3000/posts/24")
                .then().statusCode(200).log().all();
    }
}
